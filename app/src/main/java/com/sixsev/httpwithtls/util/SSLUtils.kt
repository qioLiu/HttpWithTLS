package com.sixsev.httpwithtls.util

import android.content.Context
import com.sixsev.httpwithtls.SSLParams
import java.io.InputStream
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.CertificateFactory
import javax.net.ssl.*

/**
 * DESCRIPTION
 * com.sixsev.httpwithtls.util
 * Created by six.sev on 2017/11/28.
 */
object SSLUtils {

    /**
     * SSLContext负责证书管理和信任器管理
     * 有数字证书的
     * 1.可以通过证书来生成一个信任管理器去验证特定服务器的访问。但如果访问的是别的网站则可能跑出异常， 因为证书不匹配
     *
     * 2. 另外一种方法可以不生成信任管理器而直接new 一个TrustManager来验证当前证书的合法性
     *
     * @param certificateInputStreams  客户端信任的服务器的证书
     * @param clientKeystoreInputStreams 客户端自身的密钥证书
     */
    fun getSSLFactoryWithCertification(context: Context?, certificateInputStreams: Array<InputStream>?, clientKeystoreInputStreams: Array<InputStream>?, keyPSD: String?): SSLParams{

        val sslContext = SSLContext.getInstance("TLS")

        // 证书工厂
        val certificateFactory = CertificateFactory.getInstance("X.509")
        //客户端信任的密钥库
        val keyStore = KeyStore.getInstance("BKS")
        keyStore.load(null, null)
        for(certificateInputStream in certificateInputStreams!!){
            val certificate = certificateFactory.generateCertificate(certificateInputStream)
            // 加载证书到密钥库中
            keyStore.setCertificateEntry(certificate.hashCode().toString(), certificate)
            if(certificateInputStream != null){
                certificateInputStream.close()
            }
        }

        //服务器要验证的客户端的密钥库和密码
        val keyStore_client = KeyStore.getInstance("bks")
        for(clientKeystoreInputStream in clientKeystoreInputStreams!!){
            keyStore_client.load(clientKeystoreInputStream, null)
            if(clientKeystoreInputStream != null)
                clientKeystoreInputStream.close()
        }

        //密钥管理器
        val keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
        // 加载密钥库到管理器
        keyManagerFactory.init(keyStore_client, keyPSD!!.toCharArray())

        //方法一
        //客户端信任的信任管理器 // 加载密钥库到信任管理器
        val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(keyStore)

        //方法二：
        /*val trust = arrayOf(HttpTrustManager(certificate))
        sslContext.init(keyManagerFactory.keyManagers, trust, SecureRandom())*/

        val trustManagers  = trustManagerFactory.trustManagers
        val trustX509Manager = chooseX509Manager(trustManagers)

        sslContext.init(keyManagerFactory.keyManagers, trustManagerFactory.trustManagers, SecureRandom())
        return SSLParams(sslContext.socketFactory, trustX509Manager!!)
    }

    private fun chooseX509Manager(trustManagers: Array<TrustManager>?): X509TrustManager?{
        for (trustManager in trustManagers!!){
            if(trustManager is X509TrustManager){
                return trustManager
            }
        }
        return null
    }

    /**
     * 无数字证书
     */
    fun getSSLContextWithoutCertificate(trustManager: TrustManager): SSLContext{
        var sslContext: SSLContext? = null
        try {
            sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, arrayOf(trustManager), SecureRandom())
        }catch (e: Exception){
            e.printStackTrace()
        }
        return sslContext!!
    }
}