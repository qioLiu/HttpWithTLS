package com.sixsev.httpwithtls.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sixsev.httpwithtls.HttpTrustManager
import com.sixsev.httpwithtls.R
import com.sixsev.httpwithtls.util.SSLUtils
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLSession

/**
 * 使用HttpURLConnection和OkHttp来使用TLS层进行HTTPS的请求
 * 由于HttpClient已经被废弃不做展示
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv_http_url.setOnClickListener { startHttpURLConnectionRequest() }
        tv_okhttp.setOnClickListener { startOkHttpRequest() }
    }

    private fun startOkHttpRequest() {
        val sslParams = SSLUtils.getSSLFactoryWithCertification(this, arrayOf(FileInputStream("daf.cer")), null, null)
        val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                .sslSocketFactory(sslParams.sslFactory, sslParams.sslTrustManger)
                .hostnameVerifier(HttpHostNameVerifier())
                .build()
    }

    private fun startHttpURLConnectionRequest() {
        //以下程序需要在线程中执行， 当前只是为了演示，故没有做线程执行
        val request_path = "https://www.baidu.com?city='shenzhen'"
        val url = URL(request_path)
        val openConnection = url.openConnection() as HttpURLConnection
        if(openConnection is HttpsURLConnection){
            openConnection.sslSocketFactory = SSLUtils.getSSLContextWithoutCertificate(HttpTrustManager()).socketFactory
            //SSLFactory同时可以根据访问的域名来判断是否给予访问。
            openConnection.hostnameVerifier = HttpHostNameVerifier()
        }
        /**
         * get 请求，参数已经跟在路径后面整个一起请求的。
         * 如www.baidu.com?city=sdfs&name=fsaf
         */
        openConnection.requestMethod = "GET"
        openConnection.connectTimeout = 8 * 1000
        openConnection.readTimeout = 8 * 1000
        openConnection.doInput = true
        openConnection.doOutput = true
        val responseCode = openConnection.responseCode
        if(responseCode == 200){
            val input = openConnection.inputStream
            val outStream = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            var length: Int
            try {
                do {
                    length = input.read(buffer)
                    if(length != -1) outStream.write(buffer, 0, length)
                } while (length != -1)
            }catch (e: Exception){
                //抛出约定异常给主线程去处理
            }finally {
                outStream.flush()
                outStream.close()
                input.close()
            }
        }

        /**
         * post 请求
         * 参数体以内容的形式通过流写入到服务器
         * 参数一般以HashMap的形式传过来
         */
        val params = HashMap<String, String>()
        openConnection.requestMethod = "POST"
        openConnection.doInput = true
        openConnection.doOutput = true
        val bf = StringBuffer()
        val iterator = params.iterator()
        while (iterator.hasNext()){
            bf.append("&").append(iterator.next().key).append("=").append(iterator.next().value)
        }
        if(bf.isNotEmpty()){
            bf.deleteCharAt(0)
        }
        val paramsString = bf.toString()
        val output = openConnection.outputStream
        try {
            output.write(paramsString.toByteArray())
        }catch (e: Exception){
            //抛出约定异常给主线程去处理
        }finally {
            output.flush()
            output.close()
        }
        openConnection.setRequestProperty("Content-length", paramsString.length.toString())
        if(openConnection.responseCode == 200){
            val input = openConnection.inputStream
            val outStream = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            var length: Int
            try {
                do {
                    length = input.read(buffer)
                    if(length != -1) outStream.write(buffer, 0, length)
                } while (length != -1)
            }catch (e: Exception){
                //抛出约定异常给主线程去处理
            }finally {
                outStream.flush()
                outStream.close()
                input.close()
            }
        }
    }

    inner class HttpHostNameVerifier : HostnameVerifier{
        override fun verify(p0: String?, p1: SSLSession?): Boolean {

            return true
        }
    }
}
