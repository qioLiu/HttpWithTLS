package com.sixsev.httpwithtls

import java.io.FileInputStream
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager

/**
 * DESCRIPTION
 * com.sixsev.httpwithtls
 * Created by six.sev on 2017/11/28.
 */
class HttpTrustManager : X509TrustManager {
    val certificateFactory = CertificateFactory.getInstance("X.509")
    val certificate = certificateFactory.generateCertificate(FileInputStream("xxxx.cer"))
    override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}

    override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {
        //在这里比较受信任的证书
        for (item in p0!!){
            item.checkValidity()
            try {
                item.verify(certificate.publicKey)
            }catch (e: NoSuchAlgorithmException){

            }catch (e1: InvalidKeyException){

            }catch (e2: Exception){

            }
        }
    }

    override fun getAcceptedIssuers(): Array<X509Certificate>{
        return emptyArray()
    }

}