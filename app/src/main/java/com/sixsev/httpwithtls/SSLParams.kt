package com.sixsev.httpwithtls

import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

/**
 * DESCRIPTION
 * com.sixsev.httpwithtls
 * Created by six.sev on 2017/11/30.
 */
data class SSLParams(val sslFactory: SSLSocketFactory, val sslTrustManger: X509TrustManager)